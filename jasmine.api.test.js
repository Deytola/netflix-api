const axios = require('axios');
const Jasmine = require('jasmine');

const connectors = require('./src/connectors');

const jasmine = new Jasmine();

try {
    require('dotenv').config()
} catch(e) {
    console.log(e)
}

global.apiClient = axios.create({
    baseURL: 'http://127.0.0.1:3000/api/v1',
    timeout: 15000
});


function stop(code) {
    const _code = code || 0;
    process.exit(_code);
}

jasmine.loadConfig({
    spec_dir: 'spec',
    spec_files: [
        'api/**/*[sS]pec.js',
        // 'api/src/routes/v1/movies/index.spec.js',

    ],
    stopSpecOnExpectationFailure: false,
    random: false
});

jasmine.onComplete(function(passed) {
    if(passed) {
        console.log('All specs have passed');
        stop();
    }
    else {
        console.log('At least one spec has failed');
        stop(1);
    }
});

connectors.mongoose.connect().then(() => {
    try {
        jasmine.execute();
    } catch(e) {
        console.log('JASMINE:', e)
        stop(1);
    }
})
.catch((e) => {
    console.log(e);
    console.error('mongoose could not connect to database. Stopping test!!!')

    stop(1)
})
