const { serverPort } = require('../config/config');
const express = require('express');
const app = express();
const port = serverPort || 3000;
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet')
const logger = require('morgan');

const loadRoutes = require('./loaders/routes');

const connectors = require('./connectors');

const {isEnvironmentProduction} = require("./utils/env_helper");

(async () => {
    app.disable('x-powered-by');

    // Force the server not to start listening until the database is connected.
    await connectors.mongoose.connect();
    await connectors.mongoose.initializeHelpers();

    app.use(logger('combined'));

    app.use(cors());

    if (isEnvironmentProduction()) {
        app.use(helmet());
    }

    app.use(bodyParser.json())

    app.use(bodyParser.urlencoded({ extended: false }));

    loadRoutes(app);

    app.listen(port, () => console.log(`✅ Netflix server listening on port ${port}!`))
})()

