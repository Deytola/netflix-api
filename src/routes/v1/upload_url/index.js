const express = require('express');
const router = express.Router();
const verifyAuth = require('../../../middlewares/verify-auth');
const imageController = require('../../../controllers/images.controller').controller;
const imageControllerObj = new imageController();

router.use(verifyAuth());

/**
 * @api {post} /upload_url Get a signed upload url
 * @apiName FetchSignedUploadURL
 * @apiGroup File
 * @apiVersion 0.1.0
 *
 * @apiHeader {String} Authorization Users unique access token.
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *          "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5ctI6IkpXVCJ9.eyJpZCI6IjVkMjNlMDhmMDA2YjJmNDQyNDQ0YzRiZSIsImlhdCI6MTU2adejYzMzM5OCwiZXhwIjoxNTc1NTkaMzk4LCJpc3MiOiJrYWdvLWRldiIsInN1YiI6IkxPR0lOIn0.7Eyit7tL3ns1bb-5euoHnTvDG8xZ6opOchFRccgrwyQ",
 *     }
 *
 * @apiParam {String} content_type This is the content type of the file being uploaded by the user. It could be `image/jpeg`, `image/png`, `video/mp4`, `video/mkv`, `video/avi`, etc
 *
 *
 * @apiSuccess {String} upload_url the signed url to upload images to.

 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "upload_url": "https://deytolas-netflix-bucket.s3.amazonaws.com/Movies/Adventure-1750258-1614697763166.mkv?AWSAccessKeyId=YOUR-AWS-ACCESS-KEY&Content-Type=video%2Fmp4&Expires=1614698063&Signature=jmHgEoZ0lSTDdAvYwk0IyMOu0DA%3D"
 *      }
 *
 * @apiError UnprocessableEntity Request could not be processed.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 UnprocessableEntity
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.post('/', imageControllerObj.getUploadUrl);

module.exports = router;
