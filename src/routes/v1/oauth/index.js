const express = require('express');
const oauth = express.Router();

const OauthController = require('../../../controllers/oauth.controller').controller;
const OauthControllerObj = new OauthController();

/**
 * @api {post} /oauth Authorize user
 * @apiName AuthorizeUser
 * @apiGroup Oauth
 * @apiVersion 0.1.0
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} [phone_number] User's phone number. This field is required if user's email is not provided
 * @apiParam {String} password User's password.
 * @apiParam {String} user_type The user's type.
 *
 *
 * @apiSuccess {Object} oauth The object containing the token
 * @apiSuccess {Object} user The object containing the user info
 * @apiSuccess {String} _id Id of the User.
 * @apiSuccess {String} family_name Firstname of the User.
 * @apiSuccess {String} given_name  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} user_type This is the type of the user `regular` or `admin`.
 * @apiSuccess {String} created_at The date the user was created.
 * @apiSuccess {String} updated_at The date the user's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "oauth": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNGEyMWJjNTE2OTAwYzVjZjlkOGJjMSIsInVzZXJfdHlwZSI6InJlZ3VsYXIiLCJpYXQiOjE1NjUxNDAzNDAsImV4cCI6MTU3ODEwMDM0MCwiaXNzIjoia2Fnby1kZXYiLCJzdWIiOiJMT0dJTiJ9.ibFpKaSAtoLe7f7Tvw6Zu3k_cqevEBu9A-MH115uc0w",
 *              "expiresIn": "3600h"
 *          },
 *          "user": {
 *              "user_type": "regular",
 *              "_id": "5d4a21bc516900c5cf9d8bc1",
 *              "email": "1565139388162@gmail.com",
 *              "phone_number": "08015651398",
 *              "family_name": "Adebola",
 *              "given_name": "Adetola",
 *              "created_at": "2019-08-07T00:56:28.213Z"
 *          }
 *      }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "message": "<Error message>"
 *     }
 */
oauth.post('/', OauthControllerObj.oauth);

module.exports = oauth;
