const express = require('express');
const router = express.Router();

const UserController = require('../../../../../controllers/users.controller').controller;
const userControllerObj = new UserController();

/**
 * @api {post} /users/me/change_password Change the logged in user's password
 * @apiName ChangePassword
 * @apiGroup User
 * @apiVersion 0.1.0
 *
 *
 * @apiParam {String} oldPassword The current user's old password
 * @apiParam {String} newPassword The current user's new password
 *
 * @apiSuccess {String} success A boolean indicating the request was a success. This doesn't mean the email was sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 * @apiError NotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 NotFound
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.post('/', userControllerObj.changePassword);

module.exports = router;
