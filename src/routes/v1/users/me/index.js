const express = require('express');
const router = express.Router();


const userController = require('../../../../controllers/users.controller').controller;
const userControllerObj = new userController();


/**
 * @api {get} /users/me Get the current user
 * @apiName GetMe
 * @apiGroup User
 * @apiVersion 0.1.0
 *
 * @apiHeader {String} Authorization Users unique access token.
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5ctI6IkpXVCJ9.eyJpZCI6IjVkMjNlMDhmMDA2YjJmNDQyNDQ0YzRiZSIsImlhdCI6MTU2adejYzMzM5OCwiZXhwIjoxNTc1NTkaMzk4LCJpc3MiOiJrYWdvLWRldiIsInN1YiI6IkxPR0lOIn0.7Eyit7tL3ns1bb-5euoHnTvDG8xZ6opOchFRccgrwyQ"
 *     }
 *
 * @apiSuccess {String} _id Id of the User.
 * @apiSuccess {String} family_name Firstname of the User.
 * @apiSuccess {String} given_name  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} user_type This is the type of the user `regular` or `admin`.
 * @apiSuccess {String} created_at The date the user was created.
 * @apiSuccess {String} updated_at The date the user's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "user": {
 *          "_id": "5d23e08f006b2f442444c4be",
 *          "family_name": "John",
 *          "given_name": "Doe",
 *          "email": "johndoe@gmail.com",
 *          "user_type": "regular",
 *          "created_at": "2019-07-09T00:32:15.737Z",
 *          "updated_at": "2019-07-09T00:32:15.737Z"
 *       }
 *     }
 *
 * @apiError NotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 NotFound
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.get('/', userControllerObj.getMe);

/**
 * @api {put} /users/me Update current user
 * @apiName UpdateMe
 * @apiGroup User
 * @apiVersion 0.1.0
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Content-Type": "application/json"
 *     }
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} password User's password.
 * @apiParam {Boolean} receive_email_notifications A boolean indicating if email notifications should be sent to a user
 * @apiParam {String} family_name User's family name.
 * @apiParam {String} given_name User's given name.
 * @apiParam {String} user_type This is the type of the user `regular` or `admin`. It defaults to regular
 *
 * @apiSuccess {String} _id Id of the User.
 * @apiSuccess {String} family_name Firstname of the User.
 * @apiSuccess {String} given_name  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} user_type This is the type of the user `regular` or `admin`.
 * @apiSuccess {String} created_at The date the user was created.
 * @apiSuccess {String} updated_at The date the user's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "user": {
 *              "_id": "5d23e08f006b2f442444c4be",
 *              "family_name": "John",
 *              "given_name": "Doe",
 *              "email": "johndoe@gmail.com",
 *              "user_type": "regular",
 *              "created_at": "2019-07-09T00:32:15.737Z"
 *              "updated_at": "2019-07-09T00:32:15.737Z"
 *          }
 *     }
 *
 * @apiError UnprocessableEntity Request could not be processed.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 UnprocessableEntity
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.put('/', userControllerObj.updateMe);

router.use('/change_password', require('./change_password'));
router.use('/movies', require('./movies'));


module.exports = router;
