const express = require('express');
const router = express.Router();

const userController = require('../../../../../controllers/users.controller').controller;
const userControllerObj = new userController();

/**
 * @api {get} /users/me/movies Get current user's uploaded movies
 * @apiName GetMyMovies
 * @apiGroup User
 * @apiVersion 0.1.0
 *
 * @apiHeader {String} Authorization Users unique access token.
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *          "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5ctI6IkpXVCJ9.eyJpZCI6IjVkMjNlMDhmMDA2YjJmNDQyNDQ0YzRiZSIsImlhdCI6MTU2adejYzMzM5OCwiZXhwIjoxNTc1NTkaMzk4LCJpc3MiOiJrYWdvLWRldiIsInN1YiI6IkxPR0lOIn0.7Eyit7tL3ns1bb-5euoHnTvDG8xZ6opOchFRccgrwyQ",
 *     }
 *
 *
 *
 * @apiSuccess {String} _id Id of the Movie.
 * @apiSuccess {Boolean} active A boolean indicating whether the movie is still available to watch.
 * @apiSuccess {String} title The title of the movie.
 * @apiSuccess {String} genre The genre to which the movie belongs.
 * @apiSuccess {String} source The url pointing to the video file of the movie.
 * @apiSuccess {Date} release_date The release date of the movie.
 * @apiSuccess {String} uploaded_by The user who uploaded the movie.
 * @apiSuccess {String} thumbnail The thumbnail used to display a movie art clip.
 * @apiSuccess {String} created_at The date the movie was uploaded.
 * @apiSuccess {String} updated_at The date the movie's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "movies": [{
 *             "_id": "5d23e08f006b2f442444c4be",
 *             "active": true,
 *             "title": "The Alchemist",
 *             "source": "https://<movie link>",
 *             "genre": "adventure",
 *             "release_date": "2019-07-09T00:32:15.737Z",
 *             "uploaded_by": "5d23e08f006b2f442444c4cd",
 *             "thumbnail": "https://<image link>",
 *             "created_at": "2019-07-09T00:32:15.737Z"
 *             "updated_at": "2019-07-09T00:32:15.737Z"
 *          }]
 *      }
 *
 * @apiError UnprocessableEntity Request could not be processed.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 UnprocessableEntity
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.get('/', userControllerObj.getMyMovies);

module.exports = router;
