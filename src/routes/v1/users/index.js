const express = require('express');
const router = express.Router();
const { proxyParams, verifyAuth } = require('../../../middlewares');
const userController = require('../../../controllers/users.controller').controller;


const userControllerObj = new userController();

/**
 * @api {post} /users Create a user
 * @apiName CreateUser
 * @apiGroup User
 * @apiVersion 0.1.0
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Content-Type": "application/json"
 *     }
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} password User's password. Password must contain a number, lowercase, and uppercase character
 * @apiParam {String} family_name User's family name.
 * @apiParam {String} given_name User's given name.
 * @apiParam {String} phone_number User's phone number.
 * @apiParam {String} user_type This is the type of the user `regular` or `admin`. It defaults to regular
 *
 * @apiSuccess {String} _id Id of the User.
 * @apiSuccess {String} family_name Firstname of the User.
 * @apiSuccess {String} given_name  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {String} phone_number User's phone number.
 * @apiSuccess {String} user_type This is the type of the user `regular` or `admin`.
 * @apiSuccess {String} created_at The date the user was created.
 * @apiSuccess {String} updated_at The date the user's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "oauth": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMjNlMDhmMDA2YjJmNDQyNDQ0YzRiZSIsImlhdCI6MTU2MjYzMzM5OCwiZXhwIjoxNTc1NTkzMzk4LCJpc3MiOiJrYWdvLWRldiIsInN1YiI6IkxPR0lOIn0.7Eyit7tL3ns1bb-5euoHnTvDG8xZ6opOchFRccgDNyQ",
 *              "expiresIn": "3600h"
 *          },
 *          "user": {
 *              "_id": "5d23e08f006b2f442444c4be",
 *              "family_name": "John",
 *              "given_name": "Doe",
 *              "home_address": "1, warehouse rd",
 *              "email": "johndoe@gmail.com",
 *              "user_type": "regular",
 *              "created_at": "2019-07-09T00:32:15.737Z"
 *              "updated_at": "2019-07-09T00:32:15.737Z"
 *          }
 *     }
 *
 * @apiError UnprocessableEntity Request could not be processed.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 UnprocessableEntity
 *     {
 *       "message": "<Error message>"
 *     }
 */
router.post('/', userControllerObj.createUser);

router.use('/me', verifyAuth(), require('./me'));

router.use('/:id', proxyParams(), require('./:id'));

module.exports = router;
