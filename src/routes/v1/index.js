const express = require('express');
const v1 = express.Router();
const sanitizeQuery = require('../../middlewares/sanitize-query');

v1.use(sanitizeQuery());

v1.use('/oauth', require('./oauth'))
v1.use('/movies', require('./movies'))
v1.use('/upload_url', require('./upload_url'))
v1.use('/users', require('./users'))


module.exports = v1;
