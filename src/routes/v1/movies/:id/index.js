const express = require('express');
const router = express.Router();

const MovieController = require('../../../../controllers/movies.controller').controller;
const movieControllerObj = new MovieController();

/**
 * @api {get} /movies/:id Get a movie with the specified ID
 * @apiName GetMovie
 * @apiGroup Movie
 * @apiVersion 0.1.0
 *
 * @apiHeader {String} Authorization Users unique access token.
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5ctI6IkpXVCJ9.eyJpZCI6IjVkMjNlMDhmMDA2YjJmNDQyNDQ0YzRiZSIsImlhdCI6MTU2adejYzMzM5OCwiZXhwIjoxNTc1NTkaMzk4LCJpc3MiOiJrYWdvLWRldiIsInN1YiI6IkxPR0lOIn0.7Eyit7tL3ns1bb-5euoHnTvDG8xZ6opOchFRccgrwyQ"
 *     }
 *
 * @apiParam {String} id Movie's unique ID.
 *
 *
 * @apiSuccess {String} _id Id of the Movie.
 * @apiSuccess {Boolean} active A boolean indicating whether the movie is still available to watch.
 * @apiSuccess {String} title The title of the movie.
 * @apiSuccess {String} genre The genre to which the movie belongs.
 * @apiSuccess {String} source The url pointing to the video file of the movie.
 * @apiSuccess {Date} release_date The release date of the movie.
 * @apiSuccess {String} uploaded_by The user who uploaded the movie.
 * @apiSuccess {String} thumbnail The thumbnail used to display a movie art clip.
 * @apiSuccess {String} created_at The date the movie was uploaded.
 * @apiSuccess {String} updated_at The date the movie's info was last updated.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "movie": {
 *             "_id": "5d23e08f006b2f442444c4be",
 *             "active": true,
 *             "title": "The Alchemist",
 *             "source": "https://<movie link>",
 *             "genre": "adventure",
 *             "release_date": "2019-07-09T00:32:15.737Z",
 *             "uploaded_by": "5d23e08f006b2f442444c4cd",
 *             "thumbnail": "https://<image link>",
 *             "created_at": "2019-07-09T00:32:15.737Z"
 *             "updated_at": "2019-07-09T00:32:15.737Z"
 *          }
 *     }
 *
 * @apiError NotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 NotFound
 *     {
 *       "message": "<Error message>"
 *     }
 */

router.get('/', movieControllerObj.getMovie);

module.exports = router;
