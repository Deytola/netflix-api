module.exports = {
    ENVIRONMENTS: {
        PRODUCTION: 'production',
        STAGING: 'staging',
        TEST: 'test',
        DEVELOPMENT: 'development',
    },
    MODELS: {
        USER: 'User',
        MOVIE: 'Movie'
    },
    MOVIES: {
        THUMBNAILS: 'Thumbnails'
    }
}
