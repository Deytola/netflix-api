const {ENVIRONMENTS} = require('./constants');
const {nodeEnv} = require('../../config/config');

module.exports = {
    isEnvironmentProduction: () => {
      return nodeEnv === ENVIRONMENTS.PRODUCTION;
    },
    isEnvironmentStaging: () => {
      return nodeEnv === ENVIRONMENTS.STAGING;
    },
    isEnvironmentTest: () => {
      return nodeEnv === ENVIRONMENTS.TEST;
    },
    isEnvironmentDevelopment: () => {
      return nodeEnv === ENVIRONMENTS.DEVELOPMENT;
    }
};
