const mongoose = require('mongoose');

const { GENRES } = require('./constants');


const movieSchema = new mongoose.Schema({
    active: {
        type: Boolean,
        default: true,
    },
    title: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    source: {
        type: String,
        required: true
    },
    release_date: {
        type: Date,
        required: true
    },
    genre: {
        type: String,
        enum: Object.values(GENRES),
        required: true
    },
    uploaded_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    thumbnail: {
        type: String,
        required: true
    }
},
{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
});


module.exports = mongoose.model('Movie', movieSchema);
