const GENRES = {
    ADVENTURE: 'adventure',
    THRILLER: 'thriller',
    ROMANCE: 'romance'
}

module.exports = {
    GENRES
}
