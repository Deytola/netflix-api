const bcrypt = require('bcrypt');
const {VERIFICATION_CODE} = require("./constants");

const hashPassword = async function(password) {
    const saltRounds = 10;
    return await bcrypt.hash(password, saltRounds);
}

const verifyPassword = async function(password, hashedPassword) {
    return await bcrypt.compare(password, hashedPassword);
}


const generateVerificationCode = (user_id) => {
    return new Promise(async (resolve, reject) => {
        const code = Math.floor(100000 + Math.random() * 900000);
        await mainServerRedis.set(`${VERIFICATION_CODE}_${user_id}`, JSON.stringify({code}), 'EX', 300, (err) => {
            if(err){
                console.error('Setting verification code failed: ', err);
                reject(err);
            }
            resolve(code);
        });
    });
}


module.exports = {
    hashPassword,
    verifyPassword,
    generateVerificationCode
}
