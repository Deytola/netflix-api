const USER_TYPES = {
    ADMIN: 'admin',
    REGULAR: 'regular'
}

module.exports = {
    USER_TYPES
}
