const { verifyPassword } = require('./utils');


/**
 *
 * @param {string} password the password to be verified
 * @returns a boolean indicating if the password was valid
 */
const verifyPasswordAsync = async function(password) {
    return await verifyPassword(password, this.password);
}

module.exports = {
    verifyPasswordAsync
}
