const mongoose = require('mongoose');

const { hashPassword, generateVerificationCode } = require('./utils');

const { USER_TYPES, USER_STATUSES } = require('./constants');

const instanceMethods = require('./instance-methods');

const userSchema = new mongoose.Schema({
    active: {
        type: Boolean,
        default: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    family_name: {
        type: String
    },
    given_name: {
        type: String
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    phone_number: {
        required: true,
        type: String,
        unique: true
    },
    user_type: {
        type: String,
        required: true,
        lowercase: true,
        default: USER_TYPES.REGULAR,
        enum: Object.values(USER_TYPES)
    }
},
{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
});

userSchema.method({
    ...instanceMethods
});

userSchema.pre('save', async function() {
    if (this.password && this.isModified('password') ) {
        this.password = await hashPassword(this.password);
    }
})

//Virtuals
userSchema.virtual('full_name').get(function () {
    return `${this.given_name} ${this.family_name}`;
});

userSchema.virtual('name').get(function () {
    return this.given_name || this.family_name;
});

module.exports = mongoose.model('User', userSchema);
