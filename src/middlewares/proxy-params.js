/**
 * This middleware a copy of the req.params object. This is
 * necessary because params object are only available to the middleware
 * handling it. So the handlers in other routes will not have access to it
 */

const proxyParams = () => (req, res, next) => {
    const params = req.params || {};
    const proxy = req.proxyParams || {};

    req.proxyParams = Object.assign({}, proxy, params);

    next();
}

module.exports = proxyParams;