const Error = require('../utils/errors').Errors;
const errorObj = new Error();

const InternalStatusCodes = require('../utils/internal_status_codes');

const oauthHelperObj = require('../helpers/oauth.helper');


const verifyAuth = () => {
    return (req, res, next) => {
        const authorization = req.get('Authorization');

        if (authorization) {
            if (authorization.startsWith('Bearer')) {
                try {
                    let token = authorization.split('Bearer')[1];
                    token = token.trim();

                    req.decode = oauthHelperObj.verifyToken(token);

                    next();
                } catch(e) {
                    console.log(e);
                    next(errorObj.Unauthorized(undefined, { internalStatus: InternalStatusCodes.INVALID_TOKEN }));
                }
            } else {
                next(errorObj.Unauthorized(undefined, { internalStatus: InternalStatusCodes.INVALID_TOKEN }));
            }
        } else {
            next(errorObj.Unauthorized(undefined,{ internalStatus: InternalStatusCodes.INVALID_TOKEN }));
        }
    }
}

module.exports = verifyAuth
