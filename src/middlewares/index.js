module.exports = {
    isRegular: require('./is-regular'),
    proxyParams: require('./proxy-params'),
    requireInstance: require('./require-instance'),
    verifyAuth: require('./verify-auth'),
}
