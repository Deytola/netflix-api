const lodash = require('lodash');
const {awsRegion, awsS3PublicImagesBucket} = require('../../../config/config');
const AWS = require('aws-sdk');
const {MOVIES} = require("../../utils/constants");
const Error = require('../../utils/errors').Errors;
const errorObj = new Error();


function getPublicImagesBucketName() {
	return awsS3PublicImagesBucket
}

function getS3() {
	return new AWS.S3({
		apiVersion: '2006-03-01',
        region: awsRegion
	});
}

const getFileExtensionFromContentType = (contentType) => {
    if(contentType.includes('jpeg')){
        return 'jpeg';
    }else if(contentType.includes('jpg')){
        return 'jpg';
    }else if (contentType.includes('png')){
        return 'png';
    }else if(contentType.includes('mp4')){
        return 'mp4';
    }else if(contentType.includes('mov')){
        return 'mov';
    }else if (contentType.includes('wmv')){
        return 'wmv';
    }else if(contentType.includes('avi')){
        return 'avi';
    }else if(contentType.includes('mkv')){
        return 'mkv';
    }else if(contentType.includes('webm')){
        return 'webm'
    }
    return undefined;
}

const getFilePathForS3 =  (genre, flag) => {
    return lodash.isEqual(flag, 0) ? `Movies/${lodash.capitalize(genre)}/${MOVIES.THUMBNAILS}` : `Movies/${lodash.capitalize(genre)}`;
}

const getVideoUploadURL = async function(data= {}) {
    const {
        content_type,
        genre,
    } = data;
    let flag = content_type.includes('image') ? 0 : 1;
    AWS.config.update({ region: awsRegion })
    const s3 = getS3();
    const bucketName = getPublicImagesBucketName();
    const URL_EXPIRATION_SECONDS = 300
    const randomID = parseInt(Math.random() * 10000000);
    const file_format = getFileExtensionFromContentType(content_type);

    if(!file_format) throw errorObj.BadRequest('Invalid file format');

    const Key = `${getFilePathForS3(genre, flag)}/${randomID}-${Date.now()}.${file_format}`

    // Get signed URL from S3
    const s3Params = {
        Bucket: bucketName,
        Key,
        Expires: URL_EXPIRATION_SECONDS,
        ContentType: `${content_type}`
    }
    const url = await s3.getSignedUrl('putObject', s3Params);
    if(url){
        return {url, path: url.substring(0,url.indexOf('?'))};
    }else{
        throw errorObj.NotFound();
    }
}


module.exports = {
    getVideoUploadURL
}
