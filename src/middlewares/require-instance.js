const models = {
    User: require('../db/user'),
}

const requireInstance = (modelName) => {
    const model = models[modelName];

    if (!model) {
        throw new Error('Invalid model name in require instance')
    }

    return async (req, res, next) => {
        const id = req.params.id || req.proxyParams.id;

        req.instance = await model.findOne({ _id: id });

        next();
    }
}

module.exports = requireInstance;
