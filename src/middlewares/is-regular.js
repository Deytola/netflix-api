const { USER_TYPES } = require('../db/user/constants');

const Error = require('../utils/errors').Errors;
const errorObj = new Error();

const isRegular = () => {
    return (req, res, next) => {
        const type = req.decode.user_type;

        if (type === USER_TYPES.REGULAR) {
            next()
        } else {
            next(errorObj.Unauthorized('Only regular users are allowed to access this endpoint'))
        }
    }
}

module.exports = isRegular