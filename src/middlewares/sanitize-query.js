const sanitizer = () => (req, res, next) => {
    if (req.query) {
        delete req.query['auth_level'];
        
        next();
    } else {
        next();
    }
}

module.exports = sanitizer;