const Errors = require('../../utils/errors').Errors;
const errorObj = new Errors();

const validateQuery = (query) => {
    if (!query || typeof query !== 'object')
        throw errorObj.UnprocessableEntity('Invalid query');
}

module.exports = validateQuery;