const movieQueryParser = (query, options) => {
    if (options.active) {
        query.where('active').equals(options.active)
    }

    return query;
}

module.exports = {
    parser: movieQueryParser
}
