const moviesQueryParser = require('./movies');

const PAGINATION = 30;

const buildQuery = (query, options, model = '') => {
    const page = options.page || 0;
    const limit = options.limit && options.limit < PAGINATION ? options.limit : PAGINATION;
    let _query = query;

    _query.limit(limit)

    try {
        _query.skip(parseInt(page) * limit)
    } catch(e) {}

    if (options.select) _query.select(options.select)

    if (options.populate) _query.populate(options.populate)

    if (options.sort){
        _query.sort(options.sort)
    }

    switch(model.toLowerCase()) {
        case 'movie':
            _query = moviesQueryParser.parser(_query, options)
            break;
        default:
            break;
    }

    return _query;
}

module.exports = buildQuery;
