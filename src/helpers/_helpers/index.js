const buildQuery = require('./buildQuery');
const validateQuery = require('./validate-query');

module.exports = {
    buildQuery,
    validateQuery
}