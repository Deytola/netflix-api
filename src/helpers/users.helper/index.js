const lodash = require('lodash');
const User = require('../../db/user');


const Errors = require('../../utils/errors').Errors;
const errorObj = new Errors();


const { buildQuery, validateQuery } = require('../_helpers');
const {USER_STATUSES} = require("../../db/user/constants");

const DEFAULT_FIELDS = '_id family_name given_name user_type';


const createUserAsync = async function(data = {}, options = {}) {
    const {
        email,
        family_name,
        given_name,
        password,
        phone_number,
        user_type
    } = data;

    if (!email)
        throw errorObj.UnprocessableEntity('Email missing');

    if (!phone_number)
        throw errorObj.UnprocessableEntity('Phone number missing');

    if (!password)
        throw errorObj.UnprocessableEntity('Password missing');

    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{6,}$/.test(password))
        throw errorObj.UnprocessableEntity('Password must contain a number, lowercase, and uppercase character');


    const foundUser = await getUserAsync({ $or: [ {email: email}, {phone_number: phone_number} ] });

    if (!foundUser) {
        const newUser = new User({
            email: email.trim(),
            family_name,
            given_name,
            password,
            phone_number,
            user_type
        });
        const savedUser = await newUser.save();
        return savedUser;
    } else {
        if(lodash.isEqual(`${foundUser.email.toLowerCase()}`, `${email.toLowerCase()}`)){
            throw errorObj.UnprocessableEntity('Email already exists');
        }else if(lodash.isEqual(`${foundUser.phone_number}`, `${phone_number}`)){
            throw errorObj.UnprocessableEntity('Phone number already exists');
        }
    }
}

const getUserAsync = (query, options = {}) => new Promise((resolve, reject) => {

    validateQuery(query);

    let select = options.select;

    if (!select & options.auth_level) {
        switch(options.auth_level) {
            case 2:
                select = `${DEFAULT_FIELDS} phone_number receive_email_notifications email created_at updated_at`;
                break;
            case 3:
                select = `${DEFAULT_FIELDS} email`;
                break;
            case 4:
                select = `${DEFAULT_FIELDS}`;
                break;
        }
    }

    options.select = select;

    let userQuery = User.findOne(query);

    userQuery = buildQuery(userQuery, options);

    userQuery.exec((err, user) => {
        if (err) {
            console.error(err);
            return reject(errorObj.UnprocessableEntity(err.message));
        }

        resolve(user);
    });
})

const getUsersAsync = (query, options = {}) => new Promise((resolve, reject) => {

    validateQuery(query);

    let select = options.select;

    if (!select & options.auth_level) {
        switch(options.auth_level) {
            case 2:
                select = `${DEFAULT_FIELDS} email created_at updated_at`;
            case 3:
                select = `${DEFAULT_FIELDS} email`;
            case 4:
                select = `${DEFAULT_FIELDS}`;
        }
    }

    options.select = select;

    let userQuery = User.find(query);

    userQuery = buildQuery(userQuery, options);

    userQuery.exec((err, users) => {
        if (err) return reject(errorObj.UnprocessableEntity(err.message));

        resolve(users);
    });
})

const updateUserAsync = function(query = {}, update = {}, options = {}) {
    return new Promise((resolve, reject) => {
        const {
            email,
            family_name,
            given_name,
            phone_number,
            user_type
        } = update;

        if (!email)
            throw errorObj.UnprocessableEntity('Email missing');


        if (!phone_number)
            throw errorObj.UnprocessableEntity('Phone Number missing');

        let userQuery = User.findOne(query);

        userQuery = buildQuery(userQuery, options);

        userQuery.exec(async (err, user) => {
            if (err) return reject(errorObj.UnprocessableEntity(err.message));

            try {
                if (!user) {
                    return reject(errorObj.NotFound('Invalid user id'));
                } else {
                    user.family_name = family_name;
                    user.email = email.trim();
                    user.given_name = given_name;
                    user.phone_number = phone_number;

                    user.user_type = user_type;

                    await user.save();

                    resolve(user);
                }
            } catch(e) {
                return reject(errorObj.UnprocessableEntity(e.message));
            }
        });
    })
}

const changePassword = async ({ user, oldPassword, newPassword }) => {
    const fetchedUser = await getUserAsync({ _id: user }, { select: 'password' });

    if (await fetchedUser.verifyPasswordAsync(oldPassword)) {
        fetchedUser.password = newPassword;
        await fetchedUser.save();
    } else {
        throw errorObj.UnprocessableEntity('Invalid Password')
    }
}


const removeUser = async (query) => {
    validateQuery(query);
    return await User.deleteOne(query);
}


const USER_STATUS = {...USER_STATUSES}

module.exports = {
    changePassword,
    createUserAsync,
    DEFAULT_FIELDS,
    getUserAsync,
    getUsersAsync,
    updateUserAsync,
    removeUser,
    USER_STATUS
}
