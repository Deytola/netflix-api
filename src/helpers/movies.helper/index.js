const lodash = require('lodash');
const Movie = require('../../db/movie');


const Errors = require('../../utils/errors').Errors;
const errorObj = new Errors();

const { buildQuery, validateQuery } = require('../_helpers');


const createMovieAsync = async function(data = {}) {
    const {
        title,
        source,
        genre,
        release_date,
        thumbnail,
        uploaded_by
    } = data;

    if (!title)
        throw errorObj.UnprocessableEntity('Title missing');

    if (!source)
        throw errorObj.UnprocessableEntity('Source missing');

    if (!genre)
        throw errorObj.UnprocessableEntity('Genre missing');

    if (!release_date)
        throw errorObj.UnprocessableEntity('Release date missing');

    if (!thumbnail)
        throw errorObj.UnprocessableEntity('Thumbnail missing');

    if (!uploaded_by)
        throw errorObj.UnprocessableEntity('uploaded_by missing');


    const foundMovie = await getMovieAsync({ $or: [ {title}, {release_date} ] });

    if (!foundMovie) {
        const newMovie = new Movie({
            title: title.trim(),
            source,
            genre,
            release_date,
            thumbnail,
            uploaded_by
        });

        return await newMovie.save();
    } else {
        if(lodash.isEqual(`${foundMovie.title.toLowerCase()}`, `${title.toLowerCase()}`)){
            throw errorObj.UnprocessableEntity('Movie already exists');
        }
    }
}

const getMovieAsync = (query, options = {}) => new Promise((resolve, reject) => {

    validateQuery(query);

    let select = options.select;

    options.select = select;

    let movieQuery = Movie.findOne(query);

    movieQuery = buildQuery(movieQuery, options);

    movieQuery.exec((err, movie) => {
        if (err) {
            console.error(err);
            return reject(errorObj.UnprocessableEntity(err.message));
        }
        resolve(movie);
    });
})

const getMoviesAsync = (query, options = {}) => new Promise((resolve, reject) => {

    validateQuery(query);

    let select = options.select;

    options.select = select;

    let movieQuery = Movie.find(query);

    movieQuery = buildQuery(movieQuery, options);

    movieQuery.exec((err, movies) => {
        if (err) return reject(errorObj.UnprocessableEntity(err.message));
        resolve(movies);
    });
})

const updateMovieAsync = function(query = {}, update = {}, options = {}) {
    return new Promise((resolve, reject) => {
        const {
            title,
            source,
            genre,
            release_date,
            thumbnail,
        } = update;

        let movieQuery = Movie.findOne(query);

        movieQuery = buildQuery(movieQuery, options);

        movieQuery.exec(async (err, movie) => {
            if (err) return reject(errorObj.UnprocessableEntity(err.message));

            try {
                if (!movie) {
                    return reject(errorObj.NotFound('Invalid movie id'));
                } else {
                    if (title) {
                        movie.title = title;
                    }
                    if (source) {
                        movie.source = source;
                    }
                    if (genre) {
                        movie.genre = genre;
                    }
                    if (release_date) {
                        movie.release_date = release_date;
                    }
                    if (thumbnail) {
                        movie.thumbnail = thumbnail;
                    }

                    await movie.save();

                    resolve(movie);
                }
            } catch(e) {
                return reject(errorObj.UnprocessableEntity(e.message));
            }
        });
    })
}

module.exports = {
    createMovieAsync,
    getMoviesAsync,
    getMovieAsync,
    updateMovieAsync
}
