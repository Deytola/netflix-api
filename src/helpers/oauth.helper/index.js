const {tokenIssuer, tokenExpiresIn, loginTokenSecret,
        passwordResetSecret, passwordResetTokenExpiresIn,
    tripTokenSecret} = require('../../../config/config');
const jwt = require('jsonwebtoken');

const TOKEN_ISSUER = tokenIssuer;
const TOKEN_EXPIRES_IN = tokenExpiresIn;
const ACCEPT_COMPANY_TOKEN_EXPIRES_IN = '7d';
const LOGIN_TOKEN_SECRET = loginTokenSecret;
const PASSWORD_RESET_SECRET = passwordResetSecret;
const PASSWORD_RESET_TOKEN_EXPIRES_IN = passwordResetTokenExpiresIn;
const TRIP_TOKEN_SECRET = tripTokenSecret;

const TOKEN_TYPES = {
    LOGIN: 'LOGIN',
    TRIP: 'TRIP',
    PASSWORD: 'PASSWORD',
    ACCEPT_COMPANY: 'ACCEPT_COMPANY'
}

const SECRETS = {
    [TOKEN_TYPES.LOGIN]: LOGIN_TOKEN_SECRET,
    [TOKEN_TYPES.PASSWORD]: PASSWORD_RESET_SECRET,
    [TOKEN_TYPES.TRIP]: TRIP_TOKEN_SECRET,
    [TOKEN_TYPES.ACCEPT_COMPANY]: TRIP_TOKEN_SECRET
}

const verify = ({ token, issuer, expiry, subject, secret }) => {
    const options = {};

    if (issuer) options.issuer = issuer;

    if (expiry) options.expiresIn = expiry;

    if (subject) options.subject = subject;

    return jwt.verify(token, secret, options);
}

const generate = ({ data = {}, issuer, expiry, subject, secret }) => {
    const options = {};

    if (issuer) options.issuer = issuer;

    if (expiry) options.expiresIn = expiry;

    if (subject) options.subject = subject;

    return jwt.sign({ ...data, iat: Math.floor(Date.now() / 1000) }, secret, options)
}

const generateToken = (data = {}, tokenType) => {
    switch(tokenType) {
        case TOKEN_TYPES.PASSWORD:
            return {
                token: generate({
                    data,
                    expiry: PASSWORD_RESET_TOKEN_EXPIRES_IN,
                    issuer: TOKEN_ISSUER,
                    secret: SECRETS[TOKEN_TYPES.PASSWORD],
                    subject: TOKEN_TYPES.PASSWORD,
                }),
                expiresIn: PASSWORD_RESET_TOKEN_EXPIRES_IN
            };
        case TOKEN_TYPES.TRIP:
            return {
                token: generate({
                    data,
                    expiry: TOKEN_EXPIRES_IN,
                    issuer: TOKEN_ISSUER,
                    secret: SECRETS[TOKEN_TYPES.TRIP],
                    subject: TOKEN_TYPES.TRIP,
                }),
                expiresIn: TOKEN_EXPIRES_IN
            };
        case TOKEN_TYPES.ACCEPT_COMPANY:
            return {
                token: generate({
                    data,
                    expiry: ACCEPT_COMPANY_TOKEN_EXPIRES_IN,
                    issuer: TOKEN_ISSUER,
                    secret: SECRETS[TOKEN_TYPES.ACCEPT_COMPANY],
                    subject: TOKEN_TYPES.ACCEPT_COMPANY,
                }),
                expiresIn: ACCEPT_COMPANY_TOKEN_EXPIRES_IN
            };
        default:
            return {
                token: generate({
                    data,
                    expiry: TOKEN_EXPIRES_IN,
                    issuer: TOKEN_ISSUER,
                    secret: SECRETS[TOKEN_TYPES.LOGIN],
                    subject: TOKEN_TYPES.LOGIN,
                }),
                expiresIn: TOKEN_EXPIRES_IN
            };
    }
}

const verifyToken = (token, tokenType) => {
    switch(tokenType) {
        case TOKEN_TYPES.PASSWORD:
            return verify({
                expiry: PASSWORD_RESET_TOKEN_EXPIRES_IN,
                issuer: TOKEN_ISSUER,
                secret: SECRETS[TOKEN_TYPES.PASSWORD],
                subject: TOKEN_TYPES.PASSWORD,
                token
            })
        case TOKEN_TYPES.TRIP:
            return verify({
                expiry: TOKEN_EXPIRES_IN,
                issuer: TOKEN_ISSUER,
                secret: SECRETS[TOKEN_TYPES.TRIP],
                subject: TOKEN_TYPES.TRIP,
                token
            })
        case TOKEN_TYPES.ACCEPT_COMPANY:
            return verify({
                expiry: ACCEPT_COMPANY_TOKEN_EXPIRES_IN,
                issuer: TOKEN_ISSUER,
                secret: SECRETS[TOKEN_TYPES.ACCEPT_COMPANY],
                subject: TOKEN_TYPES.ACCEPT_COMPANY,
                token
            })
        default:
            return verify({
                expiry: TOKEN_EXPIRES_IN,
                issuer: TOKEN_ISSUER,
                secret: SECRETS[TOKEN_TYPES.LOGIN],
                subject: TOKEN_TYPES.LOGIN,
                token
            })
    }
}

module.exports = {
    generateToken,
    TOKEN_TYPES,
    verifyToken
}
