const Error = require('../utils/errors').Errors;
const errorObj = new Error();

const movieHelper = require('../helpers/movies.helper');
const userHelper = require("../helpers/users.helper");



function MovieController() {}

MovieController.prototype.createMovie = async function(req, res, next) {
    try {
        const decodedToken = req.decode;

        const user = await userHelper.getUserAsync({ _id: decodedToken.id });

        if (!user)
            return next(errorObj.NotFound('No user id matches the id in the token'))

        req.body.uploaded_by = user._id;

        const movie = await movieHelper.createMovieAsync(req.body);

        res.json({
            movie
        })

    } catch (e) {
        console.error(e);
        next(e);
    }
}


MovieController.prototype.getMovie = async function(req, res, next) {
    try {

        const params = req.proxyParams || req.params;

        if (params && params.id) {
            const id = params.id;

            const movie = await movieHelper.getMovieAsync({ _id: id });

            if (!movie)
                return next(errorObj.NotFound('Movie not found'))

            res.json({
                movie
            })
        } else {
            return next(errorObj.NotFound())
        }

    } catch (e) {
        console.error(e);
        next(e);
    }
}

MovieController.prototype.updateMovie = async function(req, res, next) {
    try {
        const decodedToken = req.decode;

        const movie = await movieHelper.updateMovieAsync({ _id: decodedToken.id }, req.body);

        if (!movie)
            return next(errorObj.NotFound('Movie not found'));

        res.json({
            movie
        })

    } catch (e) {
        console.error(e);
        next(e);
    }
}

module.exports = {
    controller: MovieController
}
