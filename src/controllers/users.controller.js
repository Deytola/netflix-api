const Error = require('../utils/errors').Errors;
const errorObj = new Error();

const userHelper = require('../helpers/users.helper');

const movieHelper = require('../helpers/movies.helper');

const oauthHelper = require('../helpers/oauth.helper');


function UserController() {}

UserController.prototype.changePassword = async function(req, res, next) {
    try {
        const decodedToken = req.decode;

        const user = await userHelper.getUserAsync({ _id: decodedToken.id });

        if (!user) return next(errorObj.NotFound('User not found'));

        await userHelper.changePassword({ user: user._id, ...req.body });

        res.json({ success: true });

    } catch (e) {
        console.error(e);
        next(e);
    }
}

UserController.prototype.createUser = async function(req, res, next) {
    try {
        const user = await userHelper.createUserAsync(req.body);

        const token = oauthHelper.generateToken({ id: user.id, user_type: user.user_type });

        res.json({
            oauth: token,
            user
        })

    } catch (e) {
        console.error(e);
        next(e);
    }
}

UserController.prototype.getMe = async function(req, res, next) {
    try {
        const decodedToken = req.decode;

        const user = await userHelper.getUserAsync({ _id: decodedToken.id });

        if (!user)
            return next(errorObj.NotFound())

        res.json({
            user
        })

    } catch (e) {
        console.error(e);
        next(e);
    }
}

UserController.prototype.getMyMovies = async function(req, res, next) {
    try {
        const decodedToken = req.decode;

        const user = await userHelper.getUserAsync({ _id: decodedToken.id });

        if (!user)
            return next(errorObj.NotFound('User id does not match id in the token'));

        const movies = await movieHelper.getMoviesAsync({uploaded_by: user._id});

        res.json({
            movies
        });

    } catch (e) {
        console.error(e);
        next(e);
    }
}

UserController.prototype.getUser = async function(req, res, next) {
    try {
        const options = {
            auth_level: 2
        };
        const params = req.proxyParams || req.params;

        if (params && params.id) {
            const id = params.id;

            const user = await userHelper.getUserAsync({ _id: id }, options);

            if (!user)
                return next(errorObj.NotFound())

            res.json({
                user
            })
        } else {
            return next(errorObj.NotFound())
        }

    } catch (e) {
        console.error(e);
        next(e);
    }
}

UserController.prototype.updateMe = async function(req, res, next) {
    try {
        const options = {
            auth_level: 2
        };
        const decodedToken = req.decode;

        const user = await userHelper.updateUserAsync({ _id: decodedToken.id }, req.body, options);

        if (!user)
            return next(errorObj.NotFound())

        res.json({
            user
        })

    } catch (e) {
        console.error(e);
        next(e);
    }
}

module.exports = {
    controller: UserController
}
