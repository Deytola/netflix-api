const Error = require('../utils/errors').Errors;
const imageHelper = require('../middlewares/_helpers/index');
const errorObj = new Error();

function ImageController() {}

ImageController.prototype.getUploadUrl = async function(req, res, next) {

    try {
        req.body.user_type = req.decode.user_type;
        if (req.body && req.body.content_type && req.body.genre) {
            const upload = await imageHelper.getVideoUploadURL(req.body);
            if (!upload) {
                next(errorObj.NotFound());
            } else {
                return res.json({ upload });
            }
        }else {
            return next(errorObj.UnprocessableEntity('Invalid content_type or genre'));
        }
    } catch (e) {
        console.log(e)
        next(errorObj.UnprocessableEntity());
    }
}

module.exports = {
    controller: ImageController
}
