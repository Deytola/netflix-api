const lodash = require('lodash');

const User = require('../db/user');

const oauthHelper = require('../helpers/oauth.helper');

const validator = require('validator');

const Errors = require('../utils/errors').Errors;
const errorObj = new Errors();

function Oauth() {}

Oauth.prototype.oauth = async function(req, res, next) {
    const { email, password, phone_number } = req.body;

    if (!email && !phone_number)
        return next(errorObj.UnprocessableEntity('Either email or phone number must be provided'));

    if (!lodash.isEmpty(email) && !validator.isEmail(email))
        return next(errorObj.UnprocessableEntity('Invalid Email'));

    if (!lodash.isEmpty(phone_number) && phone_number.length < 11)
        return next(errorObj.UnprocessableEntity('Phone number must be at least 11 digits'));

    if (!password)
        return next(errorObj.UnprocessableEntity('Password missing'));

    try {
        const user = await User.findOne({ $or: [{email}, {phone_number}]}, 'created_at email phone_number family_name given_name password profile_picture user_type is_employee');

        if (user) {
            const verified = await user.verifyPasswordAsync(password);

            if (verified) {
                const token = oauthHelper.generateToken({ id: user.id, user_type: user.user_type });

                const userToJSON = user.toObject();
                delete userToJSON['password'];

                return res.json({ oauth: token, user: userToJSON });
            } else {
                return next(errorObj.Unauthorized('Invalid credentials'));
            }
        } else {
            return next(errorObj.Unauthorized('Invalid credentials'));
        }
    } catch(e) {
        return next(e);
    }
}

module.exports = {
    controller: Oauth
}
