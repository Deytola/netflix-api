const {nodeEnv} = require('../../config/config');
const express = require('express');
const routes = require('../routes');
const {isEnvironmentDevelopment, isEnvironmentTest, isEnvironmentStaging} = require("../utils/env_helper");

const loadRoutes = function(app) {
  if (isEnvironmentDevelopment() || isEnvironmentTest() || isEnvironmentStaging()) {
    app.use('/docs', express.static('docs'))
  }

  app.use('/api', routes);

  app.use((req, res, next) => {
      res.status(404);
      res.json({ message: 'Not Found' });
  })

  app.use((err, req, res, next) => {
      console.log(err);
      const error = err.error || err;

      res.status(error.statusCode || 500);

      const options = err.options || {};

      const body = {};
      const message = options.expose ? error.message : '';
      const internalStatus = options.internalStatus;

      if (message) {
          body.message = message;
      }

      if (internalStatus) {
          body.status = internalStatus;
      }

      res.json(
          Object.assign(error, body)
      );
  })
}

module.exports = loadRoutes;
