const mongoose = require('mongoose');
const {mongooseUser, mongoosePassword, mongooseDBName, mongooseHost} = require('../../config/config');

const MONGOOSE_USER = mongooseUser;
const MONGOOSE_PASSWORD = mongoosePassword;
const MONGOOSE_DB_NAME = mongooseDBName;
const MONGOOSE_HOST = mongooseHost;

const getMongoUrl = (user, password, dbName, host) => {
    if (user && password && dbName && host) {
        return `mongodb+srv://${user}:${password}@${host}/${dbName}?retryWrites=true&w=majority`
    } else {
        return 'mongodb://localhost/netflix-storage-staging'
    }
}

const connectMongoose = (() => {
    let isConnected = false;
    const url = getMongoUrl(
        MONGOOSE_USER,
        MONGOOSE_PASSWORD,
        MONGOOSE_DB_NAME,
        MONGOOSE_HOST
    );

    return {
        connect() {
            return new Promise((resolve, reject) => {
                if (!isConnected) {
                    try {
                        mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true }).then(r => {
                            return
                        }).catch((e)=>{console.error(e)})
                        const db = mongoose.connection;
                        db.on('error', (e) => {
                            console.error.bind(console, 'connection error: ', e);
                            isConnected = false;
                            reject();
                        });
                        db.once('open', function() {
                            console.log('Database connection successful!!!');
                            isConnected = true;
                            resolve();
                        });
                    } catch(e) {
                        console.error.bind(console, e);
                        reject()
                    }
                } else {
                    console.log('Mongoose already connected');
                    resolve();
                }
            })
        },
        getMongoUrl: () => url,
        initializeHelpers: async () => {
            String.prototype.isObjectId = function(str) {
                return mongoose.Types.ObjectId.isValid(str)
            }
        }
    }
})()


module.exports = {
    mongoose: connectMongoose
}
