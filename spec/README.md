# Netflix Server Tests
As of the 21st of November, 2019, all the test specs for Netflix Server are API tests.

There are different folders available and you can find what they are used for below.

**Api**
-----
This is where all the api tests reside. It mimicks the same structure of the src/routes folder. This structure allows us to easily tell which route has a test.


**Assets**
-------
This is where all the test movies, thumbnails and any other asset goes.



**Test-Helpers**
-------
This folder has any script that can be used to make testing easier. It has other subfolders.

