## **factory**
This contains scripts that can be use to create an instance of any model. Each factory can also create an instance of any model that a particular model depends on.

## **generate-token**
This folder helps generate a login token for a provided user.

## **truncate**
This folder contains scripts that can help wipe the database. To make sure different tests run without interferring with one another, there is a need to wipe the database and create instances of models that we need in our test.

## **utils**
This folder contains other helper functions.
