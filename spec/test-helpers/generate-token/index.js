const oauthHelper = require('../../../src/helpers/oauth.helper');

const generateToken = (instance = {}, tokenType) => {
    const data = { id: instance.id, user_type: instance.user_type }
    return oauthHelper.generateToken(data, tokenType);
}

generateToken.TOKEN_TYPES = oauthHelper.TOKEN_TYPES;

module.exports = generateToken;
