const User = require('../../../src/db/user');

const UserGetter = function() {}

UserGetter.prototype.getOne = async function(query){
    return await User.findOne(query);
}

UserGetter.prototype.getAll = async function(query){
    return await User.find(query);
}

module.exports = UserGetter;
