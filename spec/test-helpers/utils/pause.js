const pause = (seconds = 1) => new Promise(resolve => setTimeout(resolve, seconds * 1000));

module.exports = pause;