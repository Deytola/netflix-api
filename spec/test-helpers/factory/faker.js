const faker = require('faker');

const fakeImageUrl = () => {
    return faker.image.imageUrl();
}
const fakeFirstName = () => {
    return faker.name.firstName();
}

const fakeLastName = () => {
    return faker.name.lastName();
}

const fakePhoneNumber = () => {
    return faker.phone.phoneNumber();
}

const fakeEmailAddress = () => {
    return faker.internet.email();
}

const fakeStreetAddress = () => {
    return faker.address.streetAddress();
}

module.exports = {
    fakeImageUrl,
    fakeFirstName,
    fakeLastName,
    fakePhoneNumber,
    fakeEmailAddress,
    fakeStreetAddress
}