const User = require('../../../src/db/user');
const faker = require('faker');
const {fakeFirstName, fakeLastName} = require("./faker");

const createUser = async (userParams = {}) => {
    const fakerUser = Object.assign({
        email: faker.internet.email(),
        password: 'password',
        phone_number: faker.phone.phoneNumber(),
        given_name: fakeFirstName(),
        family_name: fakeLastName()
    }, userParams);

    const newFakeUser = new User(fakerUser);

    return await newFakeUser.save();
}

module.exports = createUser;
