const moment = require('moment');
const userFactory = require('./user');
const Movie = require('../../../src/db/movie');
const {fakeImageUrl} = require("./faker");

const createMovie = async (movieParams = {}) => {

    const fakerMovie = Object.assign({
        title: 'Roll and Move',
        genre: 'adventure',
        source: fakeImageUrl(),
        uploaded_by: await userFactory(),
        thumbnail: fakeImageUrl(),
        release_date: moment()
    }, movieParams);

    const newFakeMovie = new Movie(fakerMovie);

    return await newFakeMovie.save();
}

module.exports = createMovie;
