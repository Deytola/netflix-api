const Movie = require('../../../src/db/movie');

const truncate = async () => {
    await Movie.deleteMany({});
}

module.exports = truncate
