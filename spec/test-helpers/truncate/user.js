const User = require('../../../src/db/user');

const truncate = async () => {
    await User.deleteMany({});
}

module.exports = truncate