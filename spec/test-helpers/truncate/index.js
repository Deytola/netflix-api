const truncateUser = require('./user');
const truncateMovie = require('./movie');


const truncate = async () => {
    await Promise.all([
        truncateUser(),
        truncateMovie()
    ])
}

module.exports = {
    truncate,
    truncateUser,
    truncateMovie
}
