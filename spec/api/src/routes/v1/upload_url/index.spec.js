const { truncate } = require('../../../../../test-helpers/truncate');
const userFactory = require('../../../../../test-helpers/factory/user');
const generateToken = require('../../../../../test-helpers/generate-token');

describe("/upload_url", function() {
    let token;
    let user;

    beforeAll(async () => {
        await truncate();

        user = await userFactory();
        token =  generateToken(user)
    })

    describe('GET - Get an upload url', () => {
        it ('should get a signed upload url', async () => {
            const response = await apiClient.post('/upload_url', {
                content_type: "video/mp4",
                genre: "Adventure"
            }, {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            });
            const data = response.data;
            const status = response.status;
            const url = data.upload.url;
            expect(!!data.upload.url).toBe(true);
            expect(data.upload.path).toBe(url.substring(0, url.indexOf('?')));
            expect(status).toEqual(200);
        })

        afterAll(async () => {
            await truncate();
        })
    })
});

