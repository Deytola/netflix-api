const { truncate } = require('../../../../../test-helpers/truncate');
const {fakeImageUrl} = require("../../../../../test-helpers/factory/faker");
const userFactory = require("../../../../../test-helpers/factory/user");
const moment = require("moment");
const generateToken = require('../../../../../test-helpers/generate-token');

describe("/movies", function() {
    beforeAll(async () => {
        await truncate();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    })
    describe('POST - Create a movie', () => {
        let title;
        let source;
        let release_date;
        let thumbnail;
        let genre;
        let uploader;
        let token;

        beforeAll(async () => {
            title = 'Test Movie Title';
            source = fakeImageUrl();
            release_date = moment();
            thumbnail = fakeImageUrl();
            genre = 'adventure';
            uploader= await userFactory();
            token= await generateToken(uploader);
        })

        it ('should create movie', async () => {
            const payload =  {
                title,
                source,
                release_date,
                thumbnail,
                genre,
                uploaded_by: uploader._id
            };

            const response = await apiClient.post('/movies', payload, {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }}
            );

            const data = response.data;

            expect(!!data.movie).toBe(true);
            expect(data.movie.title).toBe(title.toLowerCase());
            expect(data.movie.source).toBe(source);
            expect(data.movie.thumbnail).toBe(thumbnail);
        })

        afterAll(async () => {
            await truncate();
        })
    })
  });

