const { truncate } = require('../../../../../../test-helpers/truncate')
const userFactory = require('../../../../../../test-helpers/factory/user');
const generateToken = require('../../../../../../test-helpers/generate-token');

describe("/users/:id", function() {
    beforeAll(async () => {
        await truncate();
    })

    describe('GET - Get a user by id', () => {
        let email;
        let family_name;
        let given_name;
        let password;
        let token;
        let user;

        beforeAll(async () => {
            email = 'emeka@afolabi.com';
            family_name = 'emeka';
            given_name = 'afolabi'
            password = 'password';

            user = await userFactory({
                email,
                family_name,
                given_name,
                password,
            });

            token =  generateToken(user)
        })

        it ('should get user', async () => {
            const response = await apiClient.get(`/users/${user._id}`, {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            })

            const data = response.data;

            expect(!!data.user).toBe(true);

            expect(data.user.email).toBe(email);
            expect(data.user.family_name).toBe(family_name);
            expect(data.user.given_name).toBe(given_name);
        })

        afterAll(async () => {
            await user.remove();
        })
    })
  });

