const { truncate } = require('../../../../../../../test-helpers/truncate');
const userFactory = require('../../../../../../../test-helpers/factory/user');
const movieFactory = require('../../../../../../../test-helpers/factory/movie');
const generateToken = require('../../../../../../../test-helpers/generate-token');

describe("/users/me/movies", function() {
    let token;
    let user;

    beforeAll(async () => {
        await truncate();
        user = await userFactory();
        token =  generateToken(user)
    })

    describe('GET - Get all the movies uploaded by the logged in user', () => {
        let movie1;
        let movie2;

        beforeAll(async () => {
            movie1 = await movieFactory({ uploaded_by: user._id, title: "Test Title"});
            movie2 = await movieFactory({ uploaded_by: user._id });
        });

        it ('should get all user\'s movies', async () => {

            const response = await apiClient.get('/users/me/movies',{
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            })

            const data = response.data;
            const firstMovie = data.movies[0];

            expect(!!data.movies).toBe(true);
            expect(data.movies.length).toBe(2);
            expect(firstMovie.uploaded_by).toEqual(user._id.toString());
            expect(firstMovie.title).toEqual(movie1.title);
        });

        afterAll(async () => {
            await truncate();
        })
    })
  });

