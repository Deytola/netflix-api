const { truncate } = require('../../../../../../test-helpers/truncate')
const userFactory = require('../../../../../../test-helpers/factory/user');
const generateToken = require('../../../../../../test-helpers/generate-token');
const {fakeImageUrl} = require("../../../../../../test-helpers/factory/faker");


describe("/users/me", function() {
    beforeAll(async () => {
        await truncate();
    })

    describe('GET - Get current', () => {
        let email;
        let family_name;
        let given_name;
        let password;
        let token;
        let user;

        beforeAll(async () => {
            email = 'emeka@afolabi.com';
            family_name = 'emeka';
            given_name = 'afolabi'
            password = 'password';

            user = await userFactory({
                email,
                family_name,
                given_name,
                password,
            });

            token =  generateToken(user)
        })

        it ('should get user', async () => {
            const response = await apiClient.get('/users/me', {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            })

            const data = response.data;

            expect(!!data.user).toBe(true);

            expect(data.user.email).toBe(email);
            expect(data.user.family_name).toBe(family_name);
            expect(data.user.given_name).toBe(given_name);
        })

        afterAll(async () => {
            await user.remove();
        })
    })

    describe('PUT - Update user', () => {
        let email;
        let family_name;
        let given_name;
        let password;
        let phone_number;
        let token;
        let user;
        let user_type;

        beforeAll(async () => {
            email = 'emeka@afolabi.com';
            family_name = 'emeka';
            given_name = 'afolabi'
            password = 'password';
            phone_number = '+234 803 123 1234';
            user_type = 'admin';

            user = await userFactory({
                email,
                family_name,
                given_name,
                password,
            });

            token =  generateToken(user)
        })

        it ('should update user', async () => {
            const payload = {
                email,
                family_name: 'test',
                given_name,
                password,
                phone_number,
                user_type
            };

            const response = await apiClient.put('/users/me', payload, {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            })

            const data = response.data;

            expect(!!data.user).toBe(true);

            expect(data.user.email).toBe(email);
            expect(data.user.family_name).toBe('test');
            expect(data.user.given_name).toBe(given_name);
            expect(data.user.user_type).toBe(user_type);
        })

        afterAll(async () => {
            await truncate();
        })
    })
  });

