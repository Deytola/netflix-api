const userFactory = require('../../../../../../../test-helpers/factory/user');
const { truncate } = require('../../../../../../../test-helpers/truncate')
const generateToken = require('../../../../../../../test-helpers/generate-token');


describe('/users/me/change_password', () => {
    let token;
    let user;

    beforeAll(async () => {
        await truncate();

        user = await userFactory({ password: 'password' });
        token = generateToken(user);
    })

    afterAll(async () => {
        await truncate();
    })

    describe('POST - Change the logged in user\'s password', () => {
        it ('should return success', async () => {
            const response = await apiClient.post('/users/me/change_password',
            {
                newPassword: 'newPassword',
                oldPassword: 'password'
            },
            {
                headers: {
                    Authorization: `Bearer ${token.token}`
                }
            })

            const data = response.data;

            expect(data.success).toBe(true);
        })
    })
})
