const { truncate } = require('../../../../../test-helpers/truncate');

describe("/users", function() {
    beforeAll(async () => {
        await truncate();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    })
    describe('POST - Create a user', () => {
        let email;
        let family_name;
        let given_name;
        let password;
        let phone_number;

        beforeAll(async () => {
            email = 'emeka@afolabi.com';
            family_name = 'Olatunde';
            given_name = 'Esther'
            password = 'Password123';
            phone_number = '+2348031234567'
        })

        it ('should create user', async () => {
            const payload =  {
                email,
                family_name,
                given_name,
                password,
                phone_number,
            };

            const response = await apiClient.post('/users', payload);

            const data = response.data;

            expect(!!data.oauth.token).toBe(true);
            expect(!!data.user).toBe(true);
            expect(data.user.email).toBe(email);
            expect(data.user.family_name).toBe(family_name);
            expect(data.user.given_name).toBe(given_name);
        })

        afterAll(async () => {
            await truncate();
        })
    })
  });

