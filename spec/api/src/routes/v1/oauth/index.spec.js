const { truncate } = require('../../../../../test-helpers/truncate');
const userFactory = require('../../../../../test-helpers/factory/user');

describe("/oauth", function() {
    beforeAll(async () => {
        await truncate();
    })

    describe('POST - Authorize a user', () => {
        let email;
        let family_name;
        let given_name;
        let password;
        let user;
        let phone_number;

        beforeAll(async () => {
            email = 'emeka@afolabi.com';
            family_name = 'emeka';
            given_name = 'afolabi'
            phone_number = '08090768663'
            password = 'password';

            user = await userFactory({
                email,
                family_name,
                given_name,
                password,
                phone_number,
            });
        })

        it ('should authenticate user', async () => {
            const response = await apiClient.post('/oauth', { phone_number, password })

            const data = response.data;

            expect(!!data.oauth.token).toBe(true);

            expect(!!data.user).toBe(true);

            expect(data.user.email).toBe(email);
            expect(data.user.family_name).toBe(family_name);
            expect(data.user.given_name).toBe(given_name);
            expect(data.user.phone_number).toBe(phone_number);
        })

        afterAll(async () => {
            await user.remove();
        })
    })
  });

