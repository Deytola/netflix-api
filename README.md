# Netflix Server
A server to enable Netflix ingest, transform and store movie data from its partners

## Environment
1. Node.js (v14+)
2. Npm (v6+)
3. Mongo (v5+)
4. Mongoose (v5.13.13)

## Running Development Server
1. Run `mongod` in your terminal
2. In another terminal, navigate to the root of the project and run `npm install`
3. Run `npm run start:dev` to start the server in the development environment.


## Running test
1. Open a terminal window and navigate to the root of the project.
2. Start the server in test environment by running `npm run start:test`.
3. Open another terminal window and navigate to the root of the project.
4. Run `npm run test:api` to run all the api tests for the project.

* The difference between the development server and the test server, is that the test server does not push anything to AWS.

## Troubleshoot
* If you try running mongod in your terminal and the command doesn't exist, you have not installed mongo properly.
* When getting weird errors when running API tests, ensure you have all the necessary environment variables set.


## Documentation
* The documentation for this api can be found at ```https://deytolas-netflix-api.herokuapp.com/docs/```
