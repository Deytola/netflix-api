const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    awsRegion: process.env.AWS_REGION,
    awsS3PublicImagesBucket: process.env.AWS_S3_PUBLIC_IMAGES_BUCKET,
    awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
    awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    passwordResetSecret: process.env.PASSWORD_RESET_SECRET,
    passwordResetTokenExpiresIn: process.env.PASSWORD_RESET_TOKEN_EXPIRES_IN,
    s3Host: process.env.S3_HOST,
    loginTokenSecret: process.env.LOGIN_TOKEN_SECRET,
    tokenIssuer: process.env.TOKEN_ISSUER,
    tokenExpiresIn: process.env.TOKEN_EXPIRES_IN,
    nodeEnv: process.env.NODE_ENV,
    serverPort: process.env.PORT,
    mongooseUser: process.env.MONGOOSE_USER,
    mongoosePassword: process.env.MONGOOSE_PASSWORD,
    mongooseDBName: process.env.MONGOOSE_DB_NAME,
    mongooseHost: process.env.MONGOOSE_HOST
};
